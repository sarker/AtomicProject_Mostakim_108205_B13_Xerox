 <?php
session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."AtomicProject_Mostakim_108205".DIRECTORY_SEPARATOR."vendor/autoload.php");
use \App\BITM\SEIP108205\Summary_Organization\Summary_Organization;
use App\BITM\SEIP108205\Message\Message;

$organization=new Summary_Organization();
$organizations=$organization->index();


?>
     

<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body >
        <div align="center";>
        <h1>Organization Summary</h1>
        <div class="warning">
            <?php
            echo Message::flash(); 
            ?>
        </div>
        <div><span>Search / Filter </span> 
            <span >Download as PDF | XL  <a href="./create.php">create New</a></span>
            <select>
                <option>10</option>
                <option>20</option>
                <option>30</option>
                <option>40</option>
                <option>50</option>
            </select>
        </div>
        <table border="1">
            <thead>
                <tr>
                    <th>Sl.</th>
                    <th>Organization Summary</th>
                    <th>Organization Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach($organizations as $organization):
                ?>
                <tr>
                    <td><?php echo $organization['id'];?></td>
                    <td><a href="show.php?id=<?php echo $organization['id'];?>"><?php echo $organization['summary'];?></a></td>
                    <td><?php echo $organization['orgname'];?></td>
                    <td><a href="edit.php?id=<?php echo $organization['id'];?>"> Edit</a> |<a href="delete.php?id=<?php echo $organization['id'];?>"> Delete</a> | Trash/Recover | Email to Friend</td>
                </tr>
                <?php
                 endforeach;
                ?>
 
            </tbody>
        </table>
        <div><span> prev  1 | 2 | 3 next </span></div>
        <a href="http://localhost/AtomicProject_Mostakim_108205/index.php">Back use absolute</a>
        <a href="../../../index.php">Back using relative path</a>
    </div>
    </body>
</html>
